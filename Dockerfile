FROM python3.12:latest

ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONBUFFERD 1

WORKDIR /app

RUN apt update && apt upgrade -y && \
    pip install --upggrade pip  && \
    pip install poetry && \
    poetry config virtualenvs.create false

COPY pyproject.toml pyproject.toml /app/

RUN poetry install --no-root --no-interaction --no-ansi

COPY . /app/

