DC = docker compose
DE = docker exec -it
DLOGS = docker logs

STORAGES_FILE = docker/docker-compose.yml

DB_CONTAINER = landing_db
DB_USER = landing
DB_NAME = ${DB_CONTAINER}


.PHONY: storages

storages:
	${DC} -f ${STORAGES_FILE} up -d

storages-stop:
	${DC} -f ${STORAGES_FILE} down

storages-logs:
	${DLOGS} ${DB_CONTAINER} -f

pg_console:
	${DE} ${DB_CONTAINER} psql -U ${DB_USER} ${DB_NAME}