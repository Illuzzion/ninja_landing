from django.contrib import admin

from reference_information.models import Speciality, EducationalSubject


# Register your models here.
@admin.register(Speciality)
class SpecialityAdmin(admin.ModelAdmin):
    pass


@admin.register(EducationalSubject)
class EducationalSubjectAdmin(admin.ModelAdmin):
    filter_horizontal = 'specialities',
