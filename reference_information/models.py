from colorfield.fields import ColorField
from django.db import models

# Create your models here.
class Speciality(models.Model):
    title = models.CharField('Название специальности', max_length=255)

    class Meta:
        verbose_name = 'Специальность'
        verbose_name_plural = 'Специальности'

    def __str__(self):
        return self.title


class EducationalSubject(models.Model):
    """Предметы для сдачи"""
    title = models.CharField('Название', max_length=255, help_text='Введите название предмета')
    icon = models.ImageField('Иконка', blank=True, null=True)
    bg_color = ColorField('Цвет фона', default='#ffffff')
    text_color = ColorField('Цвет текста', default='#000000')
    specialities = models.ManyToManyField(Speciality)

    class Meta:
        verbose_name = 'Предмет для сдачи'
        verbose_name_plural = 'Предметы для сдачи'
