from colorfield.fields import ColorField
from django.db import models

from reference_information.models import EducationalSubject


# Create your models here.
class MainScreen(models.Model):
    """
    Главный экран

    Фон
    Заголовок и подзаголовок
    """
    title = models.CharField('Заголовок', max_length=255)
    subtitle = models.CharField('Подзаголовок', max_length=255)
    background = models.ImageField('Фон', blank=True, null=True)
    bg_color = ColorField('Цвет фона', default='#FF0000')

    class Meta:
        verbose_name = 'Главный экран'
        verbose_name_plural = 'Главные экраны'

    def __str__(self):
        return self.title


class SecondScreen(models.Model):
    """
    Второй экран

    Выбор предмета и соответствующие направления для поступления

    - кнопка "Попробовать бесплатно"
    - в блоке выбора предмета доступны все предметы сервиса. Комбинация выбранных предметов определяет список
        специальностей (для поступления для которых нужен выбранный набор рпедметов).

    Требования к выбору предметов и отображению списка специальностей:
        - русский язык и математика предвыбраны за пользователя (обязательные)
        - формулировка "Выберите дополнительные предметы для сдачи"
        - требуется выбор как минимум еще 1 предмета
        - список специальностей выводится после выбора предметов без дополнительных кнопок
        - визуально должна быть ясна связка "выбираю предмет - появляется список специальностей,
            для которых эти предметы нужны"
        - список специальностей - отображается первые несколько спецальностей с возможностью развернуть список при
            нажатии на кликабельный текст "посмотреть все специальности"
    """
    title = models.CharField('Название', max_length=255)
    edu_subjects = models.ManyToManyField(EducationalSubject, related_name='screens',
                                          verbose_name='Предметы для сдачи')
    specialities_title = models.CharField('Название специальности', max_length=255)
