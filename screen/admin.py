from django.contrib import admin

from screen.models import MainScreen, SecondScreen


# Register your models here.
@admin.register(MainScreen)
class MainScreenAdmin(admin.ModelAdmin):
    list_display = ('title', 'subtitle', 'bg_color', 'background')

    def has_add_permission(self, request):
        return MainScreen.objects.count() == 0


@admin.register(SecondScreen)
class SecondScreenAdmin(MainScreenAdmin):
    pass
