from typing import Any

import orjson
from django.http import HttpRequest
from ninja import NinjaAPI, Schema
from ninja.parser import Parser
from ninja.renderers import JSONRenderer
from ninja.types import DictStrAny


class ORJONParser(Parser):
    def parse_body(self, request: HttpRequest) -> DictStrAny:
        return orjson.loads(request.body)


class ORJSONRenderer(JSONRenderer):
    def render(self, request: HttpRequest, data: Any, *, response_status: int) -> Any:
        return orjson.dumps(data, **self.json_dumps_params)


api = NinjaAPI(parser=ORJONParser(), renderer=ORJSONRenderer())


class Item(Schema):
    a: int
    b: int


class ResponseSchema(Schema):
    result: int


@api.post('/', response={200: ResponseSchema})
def add(request, data: Item):
    return {
        'result': data.a + data.b
    }


class UserSchema(Schema):
    username: str
    is_authenticated: bool
    email: str
    first_name: str
    last_name: str


class Error(Schema):
    message: str


@api.get('/me', response={200: UserSchema, 403: Error})
def me(request):
    if not request.user.is_authenticated:
        return 403, {'message': 'Please login first'}

    return request.user
