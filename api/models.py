from django.db import models

from reference_information.models import Speciality, EducationalSubject


# class Banner(models.Model):
#     """
#     Баннер с сезонным предложением
#     Возможно берется из ПС
#     """


class ServiceParagraph(models.Model):
    icon = models.ImageField('Иконка')
    title = models.CharField('Заголовок', max_length=255)
    description = models.CharField('Описание', max_length=255)

    class Meta:
        verbose_name = 'Пункт сервиса'
        verbose_name_plural = 'Пункты сервиса'


class InsideService(models.Model):
    """
    Что внутри сервиса?

    Добавить описание пунктов и иконки к каждому пункту:

    Подготовка к трудным заданиям
    получи дополнительные баллы для поступления

    Алгоритмы решений и подсказки
    будь готов к любому формату задания
    на экзамене

    Индивидуальный план подготовки
    занимайся регулярно по выстроенному расписанию

    Режим экзамена
    выполнняй задания со встроенным таймером

    Панель прогресса
    отслеживай результаты подготовки в разделе ""Мои достижения""

    Занятия от 15 минут в день
    регулируй время на подготовку в день самостоятельно"

    Элементы:
        1. Фотография студента около университета. Выбрать изображение, которое будет нести посыл "Я поступил"
        2. Доступные предметы для подготовки (аналогично текущему лендингу)
        3. 6 текстовых пунктов, сопровождение каждого пункта иконками (по смыслу текста)
    """
    service_picture = models.ImageField('Фотография студента')
    education_subjects = models.ManyToManyField(EducationalSubject, related_name='services',
                                                verbose_name='Доступные предметы для подготовки')
    paragraphs = models.ManyToManyField(ServiceParagraph, verbose_name='Пункты сервиса')
    paragraphs_count = models.IntegerField(verbose_name='Количество пунктов сервиса')

    class Meta:
        verbose_name = 'Что внутри сервиса?'


class StepScreen(models.Model):
    img = models.ImageField('Скриншот')
    title = models.CharField('Заголовок', max_length=255)
    description = models.CharField('Описание', max_length=255)
    bottom_text = models.CharField('Нижний текст', max_length=255)

    class Meta:
        verbose_name = 'Шаг'
        verbose_name_plural = 'Шаги'


class OrderedSteps(models.Model):
    step = models.ForeignKey(StepScreen, on_delete=models.CASCADE, verbose_name='Шаг')
    order = models.IntegerField(verbose_name='Порядок', default=1)
    steps_screen = models.ForeignKey('StepsToPrepare', on_delete=models.CASCADE, verbose_name='Шаги')


class StepsToPrepare(models.Model):
    """
    Три простых шага для успешной сдачи ЕГЭ

    3 экрана. Содержат:
        - скриншоты из сервиса (крупные!)
        - текстовый блок

    Смена экранов каждые 3 секунды

    Самостоятельная возможность перелистнуть экран нажатием на соответствующую кнопку

    По тексту (структура):
        - Заголовок
        - Пункты (сделать маркированным списком)
        - Мотивирующая фраза (отличается шрифтом)

    ---

    "Шаг 1. Укажи нужный для поступления балл и время на подготовку
        - построим индивидуальный план по выбранным предметам
        - выполняй контрольные и треировочные работы по графику
        - решай дополнительные задания, если осталось время
    Сделаем так, что точно успеешь подготовиться!

    Шаг 2. Самые трудные задания теперь по плечу
        - изучай алгоритмы решений, чтобы быть готовым к любому формату заданий на экзамене
        - подсказки помогут отработать стратегию выполнения задания
        - критерии оценивания покажут, какие навыки будут проверять на экзамене
    Сделаем подготовку комфортной!

    Шаг 3. Отслеживай свой прогресс в решении самых трудных заданий
        - отслеживай результаты в разделе ""мои достижения"" - какие задания получаются отлично, а по каким требуется интенсивная  подготовка
        - таймер поможет привыкнуть к формату экзамена
    Поможем превзойти самого себя!"
    """
    steps = models.ManyToManyField(OrderedSteps, verbose_name='Шаги')


class Review(models.Model):
    """
    Отзывы
    Истории успеха (вариант 1)

    Блок отображает опыт других пользователей по подготовке к ЕГЭ с нашим сервисом в формате видео.

    Элементы:
         1. видео
         2. профайл (поля захардкожены, содержание меняется в зависимости от карточки отзыва). Поля:
         - "Учится в:"
         - "Предмет: "
         - "Результат:"
         - "Время на подготовку:"

         Размещены 5 отзывов.

         Реализована возможность бесконечного перелистывания отзывов (повторяются) кликом на соответствующую кнопку (см макет Миро)

    ---
    Истории успеха (вариант 2, запасной вариант)

    Блок отображает опыт других пользователей по подготовке к ЕГЭ с нашим сервисом в формате карточек.

    Элементы:
         1. Фото в кружочке
         2. Имя и фамилия
         3. ВУЗ
         4. Текст отзыва
         5. Пол

    """
    video = models.FileField(upload_to='reviews/', verbose_name='Видео')
    username = models.CharField(max_length=100, verbose_name='Имя и фамилия')
    institute = models.CharField(max_length=100, verbose_name='ВУЗ')
    subject = models.CharField(max_length=100, verbose_name='Предмет')
    result = models.CharField(max_length=100, verbose_name='Результат')
    time_to_prepare = models.CharField('Время на подготовку')

    class Meta:
        verbose_name = 'История успеха'
        verbose_name_plural = 'Истории успеха'
